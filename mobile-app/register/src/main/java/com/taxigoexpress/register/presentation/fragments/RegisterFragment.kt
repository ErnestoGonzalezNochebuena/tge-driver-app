package com.taxigoexpress.register.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent

import com.taxigoexpress.register.R
import kotlinx.android.synthetic.main.fragment_register_layout.*

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_register_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadView()
    }

    private fun loadView() {
        this.btnRegister.clickEvent().observe(this, Observer {
            this.replaceFragment(
                DocumentsFragment.newInstance(),
                R.id.register_content,
                true
            )
        })
    }

    companion object {
        fun newInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }


}
