package taxigoexpress.com.driver.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_trips_history_layout.*
import taxigoexpress.com.driver.HomeActivity
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.presentation.adapters.TripsHistoryAdapter

/**
 * A simple [Fragment] subclass.
 */
class TripsHistoryFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_trips_history_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.loadAdapter()
    }

    private fun configureToolbar() {
        val homeActivity = this.activity as HomeActivity
        homeActivity.showSectionToolbar()
        homeActivity.changeTitleToolbar("Historial de viajes")
    }

    private fun loadAdapter() {
        val adapter = TripsHistoryAdapter()
        this.rvTripsHistory.adapter = adapter
        this.rvTripsHistory.layoutManager = LinearLayoutManager(this.context)
    }

    companion object {
        fun newInstance(): TripsHistoryFragment {
            return TripsHistoryFragment()
        }
    }

}
