package taxigoexpress.com.driver.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import taxigoexpress.com.driver.HomeActivity

import taxigoexpress.com.driver.R

/**
 * A simple [Fragment] subclass.
 */
class CurrenciesFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_currencies_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
    }

    private fun configureToolbar() {
        val homeActivity = this.activity as HomeActivity
        homeActivity.showSectionToolbar()
        homeActivity.changeTitleToolbar("Saldo y pagos")
    }

    companion object {
        fun newInstance(): CurrenciesFragment {
            return CurrenciesFragment()
        }
    }


}
