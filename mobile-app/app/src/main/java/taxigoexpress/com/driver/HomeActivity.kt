package taxigoexpress.com.driver

import android.os.Bundle
import android.view.Gravity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.presentation.activities.LoginActivity
import kotlinx.android.synthetic.main.activity_home_layout.*
import kotlinx.android.synthetic.main.content_home_toolbar.*
import kotlinx.android.synthetic.main.navigation_view.*
import kotlinx.android.synthetic.main.navigation_view_menu.*
import org.jetbrains.anko.intentFor
import taxigoexpress.com.driver.presentation.adapters.MainMenuListener
import taxigoexpress.com.driver.presentation.adapters.OptionsMenuAdapter
import taxigoexpress.com.driver.presentation.fragments.*

class HomeActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_home_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.launchMainView()
        this.configureMenu()
    }

    private fun configureMenu() {
        setSupportActionBar(home_toolbar)
        this.replaceFragment(MainTripViewFragment.newInstance(), R.id.home_container, false)
        startActivity(this.intentFor<LoginActivity>())
    }

    private fun launchMainView() {
        this.ivToolbarMenu.clickEvent().observe(this, Observer {
            this.drawer_layout.openDrawer(Gravity.LEFT)
        })

        this.ivNavigationViewMenu.clickEvent().observe(this, Observer {
            this.drawer_layout.closeDrawer(Gravity.LEFT)
        })

        val adapter = OptionsMenuAdapter(this.resources.getStringArray(R.array.menu))
        this.rvSideMenu.adapter = adapter
        this.rvSideMenu.layoutManager = LinearLayoutManager(this)
        adapter.setListener(object : MainMenuListener {
            override fun onSelectOption(option: MainMenuListener.OptionsMenu) {
                this@HomeActivity.drawer_layout.closeDrawer(Gravity.LEFT)
                val fragment = when (option) {
                    MainMenuListener.OptionsMenu.TripsHistory -> TripsHistoryFragment.newInstance()
                    MainMenuListener.OptionsMenu.Stadistics -> StadisticsFragment.newInstance()
                    MainMenuListener.OptionsMenu.Curreny -> CurrenciesFragment.newInstance()
                    else -> ProfileFragment.newInstance()
                }
                this@HomeActivity.replaceFragment(fragment, R.id.home_container, true)
            }
        })

        this.clNavigationView.setBackgroundColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )


    }

    private fun configureToolbar() {
        this.ivToolbarBack.clickEvent().observe(this, Observer {
            if (this.supportFragmentManager.backStackEntryCount > 0) {
                this.supportFragmentManager.popBackStack()
            } else {
                this.finish()
            }
        })
    }

    fun changeTitleToolbar(titleToolbar: String) {
        this.tvToolbarTitle.text = titleToolbar
    }

    fun setUserInformationToolbar(username: String, content: String) {
        this.tvToolbarUsername.text = username
        this.tvToolbarContent.text = content
    }

    fun showMainToolbar() {
        this.ivToolbarMenu.visible()
        this.llUserInformationTitle.visible()
        this.ivImageToolbar.visible()
        this.ivToolbarBack.gone()
        this.tvToolbarTitle.gone()
    }

    fun showSectionToolbar() {
        this.ivToolbarMenu.gone()
        this.llUserInformationTitle.gone()
        this.ivImageToolbar.gone()
        this.ivToolbarBack.visible()
        this.tvToolbarTitle.visible()
    }

}
