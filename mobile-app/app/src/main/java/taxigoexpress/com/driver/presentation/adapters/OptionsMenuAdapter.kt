package taxigoexpress.com.driver.presentation.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.inflate
import com.taxigoexpress.core.presentation.utils.clickEventObservable
import kotlinx.android.synthetic.main.item_menu.view.*
import taxigoexpress.com.driver.R

class OptionsMenuAdapter(private val options: Array<String>) :
    RecyclerView.Adapter<OptionsMenuAdapter.Viewholder>() {

    private var listener: MainMenuListener? = null

    fun setListener(listener: MainMenuListener) {
        this.listener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder =
        Viewholder(parent.inflate(R.layout.item_menu))


    override fun getItemCount(): Int = this.options.size

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        val option = this.options[position]
        holder.updateItem(option)
        holder.itemView.clickEventObservable().subscribe {
            when (position) {
                0 -> this.listener?.onSelectOption(MainMenuListener.OptionsMenu.TripsHistory)
                1 -> this.listener?.onSelectOption(MainMenuListener.OptionsMenu.Stadistics)
                2 -> this.listener?.onSelectOption(MainMenuListener.OptionsMenu.Curreny)
                else -> this.listener?.onSelectOption(MainMenuListener.OptionsMenu.Profile)
            }
        }
    }

    class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateItem(option: String) {
            itemView.tvMenuOption.text = option
        }
    }


}
