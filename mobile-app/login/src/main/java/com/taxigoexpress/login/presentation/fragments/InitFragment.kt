package com.taxigoexpress.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.R
import com.taxigoexpress.login.presentation.activities.LoginActivity
import com.taxigoexpress.register.presentation.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_init_layout.*
import org.jetbrains.anko.support.v4.intentFor


/**
 * A simple [Fragment] subclass.
 */
class InitFragment : BaseFragment() {


    override fun getLayout(): Int = R.layout.fragment_init_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.loadView()
    }

    private fun configureToolbar() {
        (this.activity as LoginActivity).hideToolbar()
    }

    private fun loadView() {
        this.btnGotLogin.clickEvent().observe(this, Observer {
            this.replaceFragment(
                LoginFragment.newInstance(),
                R.id.login_container,
                true
            )
//          this.activity?.intentFor<LoginActivity>()
        })

        this.btnGoRegister.clickEvent().observe(this, Observer {
            this.activity?.startActivity(intentFor<RegisterActivity>())
        })
    }

    companion object {
        fun newInstance(): InitFragment {
            return InitFragment()
        }
    }


}
