package com.taxigoexpress.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.R
import com.taxigoexpress.login.presentation.activities.LoginActivity
import kotlinx.android.synthetic.main.fragment_login_layout.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_login_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.loadView()
    }

    private fun configureToolbar() {
        (this.activity as LoginActivity).showToolbar()
    }

    private fun loadView() {
        this.btnForgotPassword.clickEvent().observe(this, Observer {
            this.replaceFragment(
                PasswordRecoveryFragment(),
                R.id.login_container,
                true
            )
        })

        this.btnLogin.clickEvent().observe(this, Observer {
            this.activity?.finish()
        })
    }

    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }


}
