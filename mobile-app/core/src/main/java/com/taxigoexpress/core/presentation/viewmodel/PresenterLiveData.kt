package com.taxigoexpress.core.presentation.viewmodel

import com.taxigoexpress.core.presentation.view.BaseLiveData

interface PresenterLiveData<T> {
    fun observeResult(): BaseLiveData<T>
}