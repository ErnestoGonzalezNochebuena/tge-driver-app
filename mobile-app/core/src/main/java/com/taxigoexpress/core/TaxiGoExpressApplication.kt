package com.taxigoexpress.core

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.components.DaggerApplicationComponent
import com.taxigoexpress.core.presentation.modules.ApplicationModule

class TaxiGoExpressApplication : MultiDexApplication(){

    val applicationComponent: ApplicationComponent by lazy{
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

}

/**
 * @return The application component from any class that have access or extends the Context class
 */
fun Context.getApplicationComponent(): ApplicationComponent =
    (this.applicationContext as TaxiGoExpressApplication).applicationComponent