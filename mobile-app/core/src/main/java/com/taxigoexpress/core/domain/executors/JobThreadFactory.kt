package com.taxigoexpress.core.domain.executors

import java.util.concurrent.ThreadFactory

class JobThreadFactory : ThreadFactory {

    private var counter: Int = 0

    override fun newThread(runnable: Runnable?): Thread = Thread(runnable, "android_" + counter++)
}